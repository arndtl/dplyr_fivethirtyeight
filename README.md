# Datenmanipulation mit dplyr: Anwendungsbeispiele mit Daten von FiveThirtyEight

Eine kurze Einführung in `dplyr` mit Daten von [FiveThirtyEight](http://www.fivethirtyeight.com) (bereitgestellt durch das `fivethirtyeight` Paket).

## Vorbereitung

Folgende Packages werden benötigt: `dplyr` und `fivethirtyeight`.

```{r}
packages <- c('dplyr', 'fivethirtyeight')

for (p in packages) {
  if (p %in% installed.packages()[,1]) require(p, character.only = T)
  else {
    install.packages(p)
    require(p, character.only = T)
  }
}
```

## Kursinhalt

- ein paar [Folien](http://arndtl.gitlab.io/dplyr_fivethirtyeight)
- ein kommentierte aber ansonsten leeres [R-Skript mit Aufgaben](https://gitlab.com/arndtl/dplyr_fivethirtyeight/blob/master/skript.R) und eine Musterlösung
